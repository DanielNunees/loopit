# README #

# Loopit coding test

## Features

- User validation and authorization
- User login, register
- JWToken for API authentication
- CRUD for table car

## Dependecies
- tymon/jwt-auth

# Before you start
##### Create your .env file
- example.env available
- set up your database credentials
##### Run the commands
```sh
$ composer install
$ php artisan key:generate
$ php artisan jwt:secret
$ php artisan migrate --seed
```


# Install
```sh
$ composer install
```
# Server run
```sh
$ php artisan serve
```

# Run tests
```sh
$ php test
```