<?php

namespace Database\Factories;

use App\Models\Roles;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$UazTkqgTiP07O2tL7fRkXusbZze8V/W7hLSY2T6cqwvWXyYlYFbr6', // abc123
            'remember_token' => Str::random(10),
        ];
    }
    public function role(string $role)
    {
        $role = Roles::factory()->count(1)->role($role)->create();
        return $this->state(['role_id' => $role->first()->id]);
    }

}
