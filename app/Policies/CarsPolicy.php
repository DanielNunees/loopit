<?php

namespace App\Policies;

use App\Models\Cars;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CarsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return ($user->isUser() or $user->isAdmin()) ? true : Response::deny('This action is unauthorized.', 403);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cars  $cars
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Cars $cars)
    {

        return $user->isAdmin() ? true : false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {

        return $user->isAdmin() ? true : Response::deny('This action is unauthorized11.', 403);

    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cars  $cars
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Cars $cars)
    {

        return $user->isAdmin() ? true : Response::deny('This action is unauthorized.', 403);

    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cars  $cars
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Cars $cars)
    {
        return $user->isAdmin() ? true : Response::deny('This action is unauthorized.', 403);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cars  $cars
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Cars $cars)
    {

        return $user->isAdmin() ? true : false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Cars  $cars
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Cars $cars)
    {

        return $user->isAdmin() ? true : false;
    }
}
