<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCarRequest;
use App\Http\Requests\UpdateCarRequest;
use App\Models\Cars;
use App\Services\CarService;
use Exception;
use Illuminate\Http\Request;

class CarsController extends Controller
{

    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->authorize('viewAny', Cars::class);
            $result = CarService::getAll();
            return response()->success($result, 200);
        } catch (Exception $ex) {
            return response()->error($ex->getMessage());
        }
    }

    public function search(Request $request)
    {
        try {

            if ($request->model == null) {
                return $this->index();
            } else {
                $result = CarService::findByModel($request->model);
                return response()->success($result, 200);
            }
        } catch (Exception $ex) {
            return response()->error($ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCarRequest $request)
    {
        try {
            $this->authorize('create', Cars::class);
            $result = CarService::create($request->validated());
            return response()->success($result, 201);
        } catch (Exception $ex) {
            return response()->error($ex->getMessage(), $ex->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCarRequest $request, Cars $car)
    {
        try {
            $this->authorize('update', $car);
            $result = CarService::update($car, $request->validated());
            return response()->success($result, 201);
        } catch (Exception $ex) {
            return response()->error($ex->getMessage(), $ex->getCode());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cars $car)
    {
        try {
            $this->authorize('delete', $car);
            $result = CarService::delete($car);
            return response()->success($result, 200);
        } catch (Exception $ex) {
            return response()->error($ex->getMessage(), (int) $ex->getCode());
        }
    }
}
