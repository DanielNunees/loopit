<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SingupRequest;
use App\Models\Roles;
use App\Models\User;
use App\Services\AuthService;
use Exception;
use Illuminate\Http\HttpResponse;

class AuthController extends Controller
{

    private $authService;
    private $user;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(AuthService $authService, User $user)
    {
        $this->authService = $authService;
        $this->user = $user;
        $this->middleware('auth:api')->except(['login', 'signup']);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return Response
     */
    public function login(LoginRequest $request)
    {
        if (!$token = auth('api')->attempt($request->only(['email', 'password']))) {
            return response()->error("Unauthorized", 401);
        }
        return response()->success($this->authService->respondWithToken($token));
    }

    /**
     * Get the authenticated User.
     *
     * @return HttpResponse
     */
    public function me()
    {
        return response()->success(auth()->user());
    }

    /**
     * Singup a new user, wiht user role id
     * @param SingupRequest $request
     * @return Response
     */
    public function signup(SingupRequest $request)
    {
        try {
            $newUser = $this->authService->singup($request->validated(), Roles::getUserRole());
            return response()->success($newUser);
        } catch (Exception $ex) {
            return response()->error($ex->getMessage());
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return Response
     */
    public function logout()
    {
        auth()->logout();
        return response()->success("Successfully logged out");
    }

    /**
     * Refresh a token.
     *
     * @return Response
     */
    public function refresh()
    {
        return $this->authService->respondWithToken(auth()->refresh());
    }

}
