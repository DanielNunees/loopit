<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    use HasFactory,Uuids;

    protected $fillable = ['maker', 'model', 'year', 'price', 'color'];
}
