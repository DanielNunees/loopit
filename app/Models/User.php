<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @mixin IdeHelperUser
 */
class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'role_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Check if the user is unique by a ginven email
     * @param string $email
     * @return bool
     */
    public function isEmailUnique(string $email)
    {
        return $this->where('email', $email)->doesntExist();
    }

    /**
     * Check if user is admin
     */
    public function isAdmin()
    {
        return $this->hasOne(Roles::class, 'id', 'role_id')
            ->where('role', 'admin')
            ->exists();
    }

        /**
     * Check if user is admin
     */
    public function isUser()
    {
        return $this->hasOne(Roles::class, 'id', 'role_id')
            ->where('role', 'user')
            ->exists();
    }

    public function role()
    {
        return $this->hasOne(Roles::class, 'id', 'role_id');
    }

}
