<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory, Uuids;

    public static function getAdminRole()
    {
        return Roles::where('role', '=', 'admin')->first();
    }

    public static function getUserRole()
    {
        return Roles::where('role', '=', 'user')->first();
    }
}
