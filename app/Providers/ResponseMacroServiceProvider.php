<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    public function boot()
    {
        /**
         * Error response
         * @param mix $data
         */
        Response::macro('success', fn($data, int $status = 200) =>
            Response::json([
                'errors' => false,
                'data' => $data,
            ], $status)
        );

        /**
         * Error response
         * @param mix $message
         * @param int $status
         */
        Response::macro('error', fn($message, int $status = 400) =>
            Response::json([
                'errors' => true,
                'message' => $message,
            ], $status)
        );
    }
}
