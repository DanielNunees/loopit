<?php

namespace App\Services;

use App\Models\Cars;

class CarService
{

    /**
     * @param array $validatedParams
     * @return Cars
     */
    public static function getAll()
    {
        return Cars::all();
    }

    /**
     * @param array $validatedParams
     * @return Cars
     */
    public static function create(array $validatedParams)
    {
        return Cars::create($validatedParams);
    }

    /**
     * @param string $car_id
     * @return Cars
     */
    public static function find(string $car_id)
    {
        return Cars::find($car_id);
    }

    /**
     * @param string $car_id
     * @return Cars
     */
    public static function findByModel(string $model)
    {
        return Cars::where('model', 'like', '%'.$model.'%')->get();
    }

    /**
     * @param Cars $car
     * @param array $validatedParams
     * @return bool
     */
    public static function update(Cars $car, array $validatedParams)
    {
        return $car->update($validatedParams);
    }

    /**
     * @param Cars $car
     * @return bool|null
     */
    public static function delete(Cars $car)
    {
        return $car->delete();
    }

}
