<?php

namespace App\Services;

use App\Models\Roles;
use App\Models\User;
use Exception;

class AuthService
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Confirms if the email address it's unique before signup the user.
     * @param array $request
     * @return User
     * @throws Exception
     */
    public function singup(array $request, Roles $role)
    {
        if ($this->user->isEmailUnique($request['email'])) {
            $this->user->name = $request['name'];
            $this->user->email = $request['email'];
            $this->user->password = bcrypt($request['password']);
            $this->user->role_id = $role->id;
            $this->user->save();
            return $this->user;
        }
        throw new Exception("The email address is already in use by another account.", 403);
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     * @return JsonResponse
     */
    public function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
        ]);
    }
}
