<?php

namespace Tests\Feature\Cars;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateCarsTest extends TestCase
{

    use RefreshDatabase, WithFaker;

    /**
     * Test if user can create a test
     *
     * @return void
     */
    public function test_create_car_if_admin()
    {
        $user = User::factory()->role('admin')->create();
        $data = [
            'maker' => $this->faker->company(),
            'model' => $this->faker->word(),
            'year' => $this->faker->year(),
            'price' => $this->faker->numberBetween(5000, 50000),
            'color' => $this->faker->safeColorName(),
        ];

        $response = $this->actingAs($user, 'api')->postJson('api/cars/', $data);
        $response->assertCreated();
    }

    /**
     * Test if user can create a test
     *
     * @return void
     */
    public function test_create_car_if_not_admin()
    {
        $user = User::factory()->role('user')->create();
        $data = [
            'maker' => $this->faker->company(),
            'model' => $this->faker->word(),
            'year' => $this->faker->year(),
            'price' => $this->faker->numberBetween(5000, 50000),
            'color' => $this->faker->safeColorName(),
        ];

        $response = $this->actingAs($user, 'api')->postJson('api/cars/', $data);
        $response->assertForbidden();
    }
}
