<?php

namespace Tests\Feature\Cars;

use App\Models\Cars;
use App\Models\User;
use Database\Seeders\CarsSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateCarsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * Test if user can create a test
     *
     * @return void
     */
    public function test_update_car_if_admin()
    {
        $user = User::factory()->role('admin')->create();
        $this->seed(CarsSeeder::class);
        $car = Cars::first();
        $data = [
            'maker' => $this->faker->company(),
            'model' => $this->faker->word(),
            'year' => $this->faker->year(),
            'price' => $this->faker->numberBetween(5000, 50000),
            'color' => $this->faker->safeColorName(),
        ];

        $response = $this->actingAs($user, 'api')->patchJson('api/cars/' . $car->id, $data);
        $response->assertCreated();
    }

    /**
     * Test if user can create a test
     *
     * @return void
     */
    public function test_update_car_if_not_admin()
    {
        $user = User::factory()->role('user')->create();
        $this->seed(CarsSeeder::class);
        $car = Cars::first();
        $data = [
            'maker' => $this->faker->company(),
            'model' => $this->faker->word(),
            'year' => $this->faker->year(),
            'price' => $this->faker->numberBetween(5000, 50000),
            'color' => $this->faker->safeColorName(),
        ];

        $response = $this->actingAs($user, 'api')->patchJson('api/cars/' . $car->id, $data);
        $response->assertForbidden();
    }
}
