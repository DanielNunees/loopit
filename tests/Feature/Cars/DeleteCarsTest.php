<?php

namespace Tests\Feature\Cars;

use App\Models\Cars;
use App\Models\User;
use Database\Seeders\CarsSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteCarsTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test if user can create a test
     *
     * @return void
     */
    public function test_delete_car_if_admin()
    {
        $this->seed(CarsSeeder::class);
        $user = User::factory()->role('admin')->create();
        $car = Cars::first();
        $response = $this->actingAs($user, 'api')->deleteJson('api/cars/' . $car->id);
        $response->assertOk();
    }

    /**
     * Test if user can create a test
     *
     * @return void
     */
    public function test_delete_car_if_not_admin()
    {
        $this->seed(CarsSeeder::class);
        $user = User::factory()->role('user')->create();
        $car = Cars::first();
        $response = $this->actingAs($user, 'api')->delete('api/cars/' . $car->id);
        $response->assertForbidden();
    }
}
