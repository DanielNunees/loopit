<?php

namespace Tests\Feature\Cars;

use App\Models\User;
use Database\Seeders\CarsSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchCarsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * Test if user can create a test
     *
     * @return void
     */
    public function test_search_car_if_admin()
    {
        $user = User::factory()->role('user')->create();
        $this->seed(CarsSeeder::class);
        $data = ['model' => 'vel'];
        $response = $this->actingAs($user, 'api')->postJson('api/cars/search', $data);
        $response->assertOk();
    }
}
