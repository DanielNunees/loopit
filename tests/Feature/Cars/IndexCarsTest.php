<?php

namespace Tests\Feature\Cars;

use App\Models\User;
use Database\Seeders\CarsSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class IndexCarsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if user can create a test
     *
     * @return void
     */
    public function test_get_all_cars_if_admin()
    {
        $this->seed(CarsSeeder::class);
        $user = User::factory()->role('user')->create();
        $response = $this->actingAs($user, 'api')->get('api/cars/');
        $response->assertOk();
    }

}
