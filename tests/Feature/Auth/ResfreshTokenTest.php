<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RefreshTokenTest extends TestCase
{
    use RefreshDatabase;

    public $expiredToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FwaVwvYXV0aFwvcmVmcmVzaCIsImlhdCI6MTYyODA4MjMwNCwiZXhwIjoxNjI4MTE4MzA0LCJuYmYiOjE2MjgwODIzMDQsImp0aSI6InFPcWRHNnNiMU5MR1Y4a2giLCJzdWIiOiI3MDFmYzQ4NS1iNzY3LTQzZDUtYjhmNS0yYmUyMzEzODQ4NTkiLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.HltnM_0oMToneN0UihxH-zcJlyX2MSF5GNYR_3A_ooQ";

    /**
     * Refresh token test
     *
     * @return void
     */
    public function test_api_refresh_token()
    {
        $user = $this->seed(UserSeeder::class);
        $user = User::first();
        $response = $this->postJson('/api/auth/login', ['email' => $user->email, 'password' => 'abc123']);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $response['data']['original']['access_token'],
        ])->postJson('/api/auth/refresh');

        $response
            ->assertOk()
            ->assertJson([
                'access_token' => true,
            ]);
    }

    /**
     * Refresh token test with expired token
     *
     * @return void
     */
    public function test_api_refresh_token_with_expired_token()
    {
        $response = $this->withHeaders([
            'Authorization' => "Bearer {$this->expiredToken}",
        ])->postJson('/api/auth/refresh');
        $response
            ->assertStatus(401);
    }
}
