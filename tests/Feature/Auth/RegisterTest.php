<?php

namespace Tests\Feature\Auth;

use App\Models\Roles;
use Database\Seeders\RolesSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Basic user register test
     *
     * @return void
     */
    public function test_api_register()
    {
        $this->seed(RolesSeeder::class);
        $data = [
            'email' => 'daniel@nunes.com',
            'password' => 'X12%$11as',
            'password_confirmation' => 'X12%$11as',
            'name' => 'Daniel nunes'];

        $response = $this->postJson('/api/auth/signup', $data);
        $response
            ->assertOk()
            ->assertJson([
                'data' => true,
            ]);
    }

    /**
     * User register test when password and its confirmation does not match
     *
     * @return void
     */
    public function test_api_register_confirmation_password_does_not_match()
    {
        $data = [
            'email' => 'daniel@nunes.com',
            'password' => 'X12%$11as',
            'password_confirmation' => 'X12%$1111',
            'name' => 'Daniel nunes'];

        $response = $this->postJson('/api/auth/signup', $data);
        $response->assertJsonFragment(
            ['errors' => ["password" => ["The password confirmation does not match."]]],
        )->assertStatus(422);

    }

    /**
     * User register test with invalid email
     *
     * @return void
     */
    public function test_api_register_with_invalid_email()
    {
        $data = [
            'email' => 'danielnunes.com',
            'password' => 'X12%$11as',
            'password_confirmation' => 'X12%$11as',
            'name' => 'Daniel nunes'];
        $response = $this->postJson('/api/auth/signup', $data);
        $response->assertJsonFragment(
            ['errors' => ["email" => ["The email must be a valid email address."]]],
        )->assertStatus(422);
    }

    /**
     * User register test without a name
     *
     * @return void
     */
    public function test_api_register_without_name()
    {
        $data = [
            'email' => 'daniel@nunes.com',
            'password' => 'X12%$11as',
            'password_confirmation' => 'X12%$11as'];
        $response = $this->postJson('/api/auth/signup', $data);
        $response->assertJsonFragment(
            ['errors' => ["name" => ["The name field is required."]]],
        )->assertStatus(422);
    }

    /**
     * User register test with password containing only letters
     *
     * @return void
     */
    public function test_api_register_with_bad_password_only_letters()
    {
        $data = [
            'email' => 'daniel@nunes.com',
            'password' => 'abcdefgh',
            'password_confirmation' => 'abcdefgh',
            'name' => 'Daniel nunes'];
        $response = $this->postJson('/api/auth/signup', $data);
        $response->assertJsonFragment(
            ['errors' => [
                "password" => [
                    "The password must contain at least one symbol.",
                    "The password must contain at least one uppercase and one lowercase letter.",
                    "The password must contain at least one number.",
                ],
            ],
            ],
        )->assertStatus(422);
    }

    /**
     * User register test with password containing only numbers
     *
     * @return void
     */
    public function test_api_register_with_bad_password_only_numbers()
    {
        $data = [
            'email' => 'daniel@nunes.com',
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'name' => 'Daniel nunes'];
        $response = $this->postJson('/api/auth/signup', $data);
        $response->assertJsonFragment(
            ['errors' => [
                "password" => [
                    "The password must contain at least one symbol.",
                    "The password must contain at least one letter.",
                    "The password must contain at least one uppercase and one lowercase letter.",
                ],
            ],
            ],
        )->assertStatus(422);
    }

    /**
     * User register test with password containing only numbers and lowercase letters
     *
     * @return void
     */
    public function test_api_register_with_bad_password_numbers_and_lowercase_letters()
    {
        $data = [
            'email' => 'daniel@nunes.com',
            'password' => '1234abcd',
            'password_confirmation' => '1234abcd',
            'name' => 'Daniel nunes'];
        $response = $this->postJson('/api/auth/signup', $data);
        $response->assertJsonFragment(
            ['errors' => [
                "password" => [
                    "The password must contain at least one symbol.",
                    "The password must contain at least one uppercase and one lowercase letter.",
                ],
            ],
            ],
        )->assertStatus(422);
    }

    /**
     * User register test with password containing only numbers  lowercase letters and uppercase letters
     *
     * @return void
     */
    public function test_api_register_with_bad_password_numbers_and_lowercase_and_uppercase_letters()
    {
        $data = [
            'email' => 'daniel@nunes.com',
            'password' => '1234ABcd',
            'password_confirmation' => '1234ABcd',
            'name' => 'Daniel nunes'];
        $response = $this->postJson('/api/auth/signup', $data);
        $response->assertJsonFragment(
            ['errors' => [
                "password" => [
                    "The password must contain at least one symbol.",
                ],
            ],
            ],
        )->assertStatus(422);
    }
}
