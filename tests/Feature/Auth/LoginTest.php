<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Basic login test
     *
     * @return void
     */
    public function test_api_login()
    {
        $this->seed(UserSeeder::class);
        $user = User::first();
        $userData = ['email' => $user->email, 'password' => 'abc123'];
        $response = $this->postJson('/api/auth/login', $userData);
        $response->assertOk()->assertJson(['data' => true]);
    }

    /**
     * Login test with invalid password
     *
     * @return void
     */
    public function test_api_login_invalid_password()
    {
        $user = $this->seed(UserSeeder::class);
        $user = User::first();
        $userData = ['email' => $user->email, 'password' => 'wrong_password'];
        $response = $this->postJson('/api/auth/login', $userData);

        $response->assertJsonFragment(
            ['message' => 'Unauthorized'],
        )->assertStatus(401);
    }

    /**
     * Login test with invalid email
     *
     * @return void
     */
    public function test_api_login_invalid_email()
    {
        $userData = [
            'email' => "invalidemail@example.com",
            'password' => '123abc',
        ];
        $response = $this->postJson('/api/auth/login', $userData);

        $response->assertJsonFragment(
            ['message' => 'Unauthorized'],
        )->assertStatus(401);
    }

    /**
     * Login test without email
     *
     * @return void
     */
    public function test_api_login_without_email()
    {
        $data = [
            'password' => '123abc',
        ];
        $response = $this->postJson('/api/auth/login', $data);

        $response->assertJsonFragment(
            ['errors' => [
                "email" => [
                    "The email field is required.",
                ],
            ],
            ],
        )->assertStatus(422);
    }

    /**
     * Login test without password
     *
     * @return void
     */
    public function test_api_login_without_password()
    {
        $data = [
            'password' => '123abc',
        ];
        $response = $this->postJson('/api/auth/login', $data);
        $response->assertJsonFragment(
            ['errors' => [
                "email" => [
                    "The email field is required.",
                ],
            ]],
        )->assertStatus(422);
    }
}
