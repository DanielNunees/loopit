<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Basic logout Test
     *
     * @return void
     */
    public function test_api_logout()
    {
        $user = $this->seed(UserSeeder::class);
        $user = User::first();
        $response = $this->postJson('/api/auth/login', ['email' => $user->email, 'password' => 'abc123']);
        $JWTtoken = $response['data']['original']['access_token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $JWTtoken,
        ])->postJson('/api/auth/logout');

        $response
            ->assertOk()
            ->assertJsonFragment([
                'data' => 'Successfully logged out',
            ]);
    }

    /**
     * Logout and try to access a protected route  with the invalidaded token
     *
     * @return void
     */
    public function test_api_logout_and_try_to_login_again_with_same_token()
    {
        $user = $this->seed(UserSeeder::class);
        $user = User::first();
        $response = $this->postJson('/api/auth/login', ['email' => $user->email, 'password' => 'abc123']);
        $JWTtoken = $response['data']['original']['access_token'];

        $logoutResponse = $this->withHeaders([
            'Authorization' => 'Bearer ' . $JWTtoken,
        ])->postJson('/api/auth/logout');

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $JWTtoken,
        ])->postJson('/api/auth/me');

        $logoutResponse
            ->assertOk()
            ->assertJsonFragment([
                'data' => 'Successfully logged out',
            ]);
        $response
            ->assertStatus(401);

    }
}
