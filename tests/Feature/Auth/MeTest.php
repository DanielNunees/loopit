<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Try to get information about the user
     *
     * @return void
     */
    public function test_api_me()
    {
        $user = $this->seed(UserSeeder::class);
        $user = User::first();
        $response = $this->postJson('/api/auth/login', ['email' => $user->email, 'password' => 'abc123']);
        $JWToken = $response['data']['original']['access_token'];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $JWToken,
        ])->postJson('/api/auth/me');

        $response
            ->assertOk()
            ->assertJson([
                'data' => true,
            ]);
    }
}
